# Digital marketing

>According to ITM (institute of direct and digital marketing) digital marketing includes the management and execution of marketing using electronic media such as web, email, interactive TV, wireless media, in congestion with digital data about customers characteristics  and behavior of planning and execution using electronic media.

The unique differentiators from the definition are explained below:

* Digital marketing involves reaching out to potential customers though set of interactive, intuitive  and advanced electronic devices and platforms which go beyond normal web and email to include mobiles, interactive TV, applications, social media, which were not common earlier to marketing online through e-models.This explosion of platforms and devices have been possible due to the rapid growth of mobile technologies and successive probations which have meant instruments like smartphones in the hands of large audiences with deadly accessible information.

* Application of marketing to consumer interactions that integrate digital data about customers and there behaviour in a manner which was not extracted and targeted earlier. This has resulted into whole new eco system of advanced technology to target each valued customer customised with messages and promotions specific to their needs and intend.

* Introduction of interactive communication channels like search, display advertising, social media etc which have strong combination of pull-push marketing rather than earlier models which were primarily of push marketing nature. incorporation of variety of technological advances in communication, social integrations, machine learning algorithms along with their applications, multiple devices and platforms, all those have made it  possible for digital to carve itself as an established area of marketing.


### Evolution of digital marketing:

1. **Trade Era-** During this period products were handmade, hence supply were very limited.

2. **Production Era-** Products were mass produced and consumers focused on features like low cost and availability. 

3. **Product Era-** Product era focus moved to quantify from quality and consumers led more emphasis on quality, performance, and inaugurate features.

4. **Sales Era-** With increasing competition, companies were compelled to emphasize on aggressive selling and promotion, commoditisation of products, leading to saturation  of consumer demands. The unique feature of production era and sales era is that it the manufacturer who is deciding everything


### Digital marketing types:

1. **Intend based marketing-** Marketing through messages placed in conjunction with information obtained on search engine queries. 

2. **Brand marketing-** Utilising advertising across websites and digital media formats for marketing.
3. **Content marketing-** Using story based elements to share marketing messages in a targeted fashion.

4. **Commuting based marketing-** Utilising social communities, networks and platforms to conduct marketing.

5. **Partner marketing-** This includes involvement of affiliates third party sides for marketing and includes sponsorship and PR(public relation) activities.

6. **Communication channel marketing-** This involves marketing on all communication on lead platforms, this is more recent and upcoming digital marketing area.

7. **Platform based marketing-** Using new platforms and digitize traditional platforms to integrate marketing in the device and medium itself.

# Search Engine Optimization

SEO consist of all activities and techniques applied towards making a website and inclusive webpages. Rank at the top of any search engines organic result listing. the prime aim of set exercise for marketer is to help generate awareness for its brand websites by aiming attention through content relevance and keyword prominence rather than paying for it through SEM campaign.

To understand the basics behind SEO they first need to develop an understanding of e-classifications like white hat and black hat SEO, on page and off page optimisation and key concept areas of website execution.

>__White hat SEO__ includes the website following search engine guidelines, developing good quality, useful content and webpages and ensuring that the webpages are built for customers rather than search engines.

>__Black hat SEO__ also known as spam texting includes practices like paying for link building, keyword stuffing, cloaked pages including hidden texts and links among others.

Top of the order companies understand typically makes sure that clearly understands and follows specific search engine, web master guidelines so that they are not unnecessarily analysed and take time to again retain their ranking status.

### Basics of on page and off page optimisation :

On page optimisation- It includes all the activities which can be executed on the webpages them self for SEO.
These are parameters that typically can be controlled by coding on the page.  Key activities includes keyword development, meta tag development including the quality content and webpage interlinking.

Off page optimisation- It covers actives that takes place outside webpages which cannot be controlled just through page optimisation. key examples includes :  linked building, increasing link popularity directories, leveraging social interactions, guest blogging etc.


##### On Page optimisation:

1. Understanding SEO concept.
2. On page optimisation impact parameters.
3. search indexing and website navigation.
4. webpage tab management
5. Quality content and keyword inclusion.
6. content discovery and linked pages.
7. Usability of user experience impact.

##### Off Page optimisation:

1. Linked building.
2. Social reputation
3. Website authority and trust
4. Personalisation and localisation

### On page optimisation parameters: 

Search engine submission - it is essential to first be listed in e-search engines and directories like Google, and Yahoo, so that when users search for watch specific	 content related to the site, the website’s name comes up. The first action point, hence would be to submit the website and business related details to measure such engines, and get them included in their lists.

>HOW TO REGISTER MY WEBSITE ONLINE ??

Website Navigation: - Here we need to create a robust and easy guided navigation structure, which starts from the home page and maps the users journey towards discovering e-products and services. Navigation  structures which have and intuitive and logical flow are good for search engines

>WEBSITE STRUCTURE — MEANS WHAT ?

URL structure - easy to follow file name and URL structure and keys to support search engines undraping better since urls are individual addresses for documents on the web and also an integral part of search and copy, a well planned URL structure can also hep improve search rating if they include a descriptive use of keywords

### What is meta tag?

The Meta tag was created as a sort of catch all. These tags allow webmasters to issue an unlimited variety of commands or to provide information to a browser, search engine or automated programme. Meta tags are contained in the head section of the HTML page. They are not visible to the users but if not placed in this section they will not be read by the search engines.	Although one can list as many keywords as they like. Most search engines will not read more than about 1000 charecters. Hence, it is recommended to include only the most important keywords at the start of the tag.

### Use of Meta Data
 * To summarize the meaning of data
 * To Allow users to search for the data
 * To Allow users to determine if the data is what they want
 * Prevent some user from accessing data
 * To instruct how to interpret data
 * To give contact information about the data
 * To indicate relationships with other resources
 * To give history of data
 

### Discuss link building

Link building is an art, it is always the most challenging part of an SEO’s job but also the most critical success. Link building also requires creativity, hustle, and offer a budget. No link building campaigns are the same and the way you choose to build links depends as much upon the website as it does your personality. Below our three basic types link acquisition.

1. __Natural editorial links-__ Links that are given natural by sites and pages that want to link to your content or company. These links requires no specific action from SEO, other than the creation of worthy material that is great content and the ability creates awareness about it.
2. __Manual outreach link building-__ The SEO creates this links by emailing bloggers for links, submitting sites to directories or pain for listening of any kind. The SEO often creates value for position by explaining to the link target while creating the link in the best interest.
3. __Self Created, not editorial-__ Hundreds of thousands of website offer any digital opportunities to create links through guest book signing, forum signature, blog comments or user profiles. This links offers the lowest values, but can aggregate. Still have an impact for some sites. In general search engines continue to devalue most of these types of links, and have been known to analyse sites that pursue these links aggressively.


__How to build link building campaign:__
As with many marketing activity the first name in any link building campaign is the creation of goals and strategies.

Unfortunately link building is one of the most difficult activities. It is impossible to those on the outside to access the information. SEO rely on the number of the signals to help building the rating scale of link values, along with the data from the link signals mentioned below.

1. Ranking for relevant search term.
2. Domain authority.
3. Competitors backlinks.
4. Number of links on the webpage.
5. Potential reference webpage.

### Basics of website analytics
The fundamental goal of web analytics has been to evaluate the website performance to collection and analysis of data related to web traffic and usage patterns. 

According to Eric Peterson web analytics demystified, web analytics includes assessment of the following types of the data to help create and generalise the experience of online visitors :

1. __Web traffic data :__ It includes the data mind from web server log files and java script page tags.
2. __Transactional data :__ It refers to business data related to transactions like number of customers and number of orders, average size of transactions etc.
3. __Web server performance data :__ It includes data related to usage based character of webpages including page weight.
4. __Usability studies :__ It refers to data in relational rules according to the visitors usage of website and key interactions.
5. __User submitted information :__ It refers to information collated through online forms and surveys.





